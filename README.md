# Personal dotfiles
These are my personal dotfiles for things, table of contents and perhaps a script will help you put all the files in their right place

# Table of contents
## Neovim
    * init.vim - ~./config/nvim/init.vim
    * coc.vim - ~./config/nvim/coc.vim
## Sway
    * config - ~/.config/sway/config
    * status - ~/.config/sway/status
## Podcasts
    * podcasts - Import with Antennapod, Gnome Podcasts, etc


" Personal vimrc Set up Vundle
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Vundle plugins
Plugin 'rust-lang/rust.vim'
Plugin 'preservim/nerdtree'
Plugin 'dracula/vim', { 'name': 'dracula' }
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tomasiser/vim-code-dark'
call vundle#end()

" Set up vim-plug
call plug#begin('~/.vim/plugged')

" Vim-plug plugins
Plug 'sheerun/vim-polyglot'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'arcticicestudio/nord-vim'
Plug 'calviken/vim-gdscript3'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'bluz71/vim-moonfly-colors'
Plug 'jacoborus/tender.vim'
Plug 'joshdick/onedark.vim'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
call plug#end()

" Standard vim config
" Set hybrid numbering on
set number relativenumber
set nu rnu

" Syntax highlighting
syntax on
syntax enable

" Indendation
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent

" Allows for ALT+{h,j,k,l} to navigate windows from any mode
:tnoremap <A-h> <C-\><C-N><C-w>h
:tnoremap <A-j> <C-\><C-N><C-w>j
:tnoremap <A-k> <C-\><C-N><C-w>k
:tnoremap <A-l> <C-\><C-N><C-w>l
:inoremap <A-h> <C-\><C-N><C-w>h
:inoremap <A-j> <C-\><C-N><C-w>j
:inoremap <A-k> <C-\><C-N><C-w>k
:inoremap <A-l> <C-\><C-N><C-w>l
:nnoremap <A-h> <C-w>h
:nnoremap <A-j> <C-w>j
:nnoremap <A-k> <C-w>k
:nnoremap <A-l> <C-w>l

" Turn off numbering when terminal opens
au TermOpen * setlocal nu! rnu!

" Bind <Esc> to exit terminal mode
:tnoremap <Esc> <C-\><C-n>

" Compile LaTeX when saved
:autocmd BufWritePost *.tex silent! !./compile <afile>

" Plugin config
" Nerdtree 
map <C-n> :NERDTreeToggle<CR>

" Oceanic-next theme
" For Neovim 0.1.3 and 0.1.4
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Or if you have Neovim >= 0.1.5
if (has("termguicolors"))
 set termguicolors
endif

" Theme
colorscheme codedark

" Onedark theme settings
let g:onedark_termcolors = 256
let g:onedark_terminal_italics = 1

" Airline theme
let g:airline_theme = 'codedark'

" coc config example
source ~/.config/nvim/coc.vim 
